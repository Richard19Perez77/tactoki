package tac.toki.puzzleimage.puzzle;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import tac.toki.puzzleimage.R;
import tac.toki.puzzleimage.common.CommonVariables;

/**
 * A fragment to show the list of stats in text views to the user.
 * Use the {@link PuzzleStatsFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PuzzleStatsFragment extends Fragment {

    public final static String TAG = "tac.toki.puzzleimage.puzzle.PuzzleStatsFragment";

    CommonVariables commonVariables = CommonVariables.getInstance();

    TextView puzzlesSolvedTextView;
    TextView twoXtwoPuzzleSolvedCountTextView;
    TextView threeXthreePuzzleSolvedCountTextView;
    TextView fourXfourPuzzleSolvedCountTextView;

    TextView fiveXfivePuzzleSolvedCountTextView;
    TextView sixXsixPuzzleSolvedCountTextView;
    TextView sevenXsevenPuzzleSolvedCountTextView;
    TextView fourRecordSolveTimeTextView;

    TextView nineRecordSolveTimeTextView;
    TextView sixteenRecordSolveTimeTextView;
    TextView twentyfiveRecordSolveTimeTextView;
    TextView thirtysixRecordSolveTimeTextView;

    TextView fourtysevenRecordSolveTimeTextView;
    TextView puzzlesSavedTextView;
    TextView blogLinksTraversedTextView;
    TextView musicSavedTextView;

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PuzzleStatsFragment.
     */
    public static PuzzleStatsFragment newInstance() {

        PuzzleStatsFragment fragment = new PuzzleStatsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //allow menu options to be added
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.main_stats, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_puzzle_stats, container, false);

        puzzlesSolvedTextView = (TextView) view.findViewById(R.id.puzzlesSolvedCountTextView);

        twoXtwoPuzzleSolvedCountTextView = (TextView) view.findViewById(R.id.twoXtwoPuzzlesSolvedTextView);
        fourRecordSolveTimeTextView = (TextView) view.findViewById(R.id.twoXtwoSolveTimeTextView);

        threeXthreePuzzleSolvedCountTextView = (TextView) view.findViewById(R.id.threeXthreePuzzlesSolvedTextView);
        nineRecordSolveTimeTextView = (TextView) view.findViewById(R.id.threeXthreeSolveTimeTextView);

        fourXfourPuzzleSolvedCountTextView = (TextView) view.findViewById(R.id.fourXfourPuzzlesSolvedTextView);
        sixteenRecordSolveTimeTextView = (TextView) view.findViewById(R.id.fourXfourSolveTimeTextView);

        fiveXfivePuzzleSolvedCountTextView = (TextView) view.findViewById(R.id.fiveXfivePuzzlesSolvedTextView);
        twentyfiveRecordSolveTimeTextView = (TextView) view.findViewById(R.id.fiveXfiveSolveTimeTextView);

        sixXsixPuzzleSolvedCountTextView = (TextView) view.findViewById(R.id.sixXsixPuzzlesSolvedTextView);
        thirtysixRecordSolveTimeTextView = (TextView) view.findViewById(R.id.sixXsixSolveTimeTextView);

        sevenXsevenPuzzleSolvedCountTextView = (TextView) view.findViewById(R.id.sevenXsevenPuzzlesSovedTextView);
        fourtysevenRecordSolveTimeTextView = (TextView) view.findViewById(R.id.sevenXsevenSolveTimeTextView);

        puzzlesSavedTextView = (TextView) view.findViewById(R.id.imageSavedTextView);

        blogLinksTraversedTextView = (TextView) view.findViewById(R.id.blogLinksTraversedTextView);

        musicSavedTextView = (TextView) view.findViewById(R.id.musicSavedTextView);

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        updatePuzzleStats();
    }

    /**
     * Runtime method to update view objects, run on UI thread.
     */
    public void updatePuzzleStats() {
        Activity act = getActivity();
        act.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String temp = "" + commonVariables.puzzlesSolved;
                if (puzzlesSolvedTextView != null)
                    puzzlesSolvedTextView.setText(temp);

                temp = "" + commonVariables.fourPiecePuzzleSolvedCount;
                if (twoXtwoPuzzleSolvedCountTextView != null)
                    twoXtwoPuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.fourRecordSolveTime / 1000.0 + " sec.";
                if (fourRecordSolveTimeTextView != null)
                    fourRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.ninePiecePuzzleSolvedCount;
                if (threeXthreePuzzleSolvedCountTextView != null)
                    threeXthreePuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.nineRecordSolveTime / 1000.0 + " sec.";
                if (nineRecordSolveTimeTextView != null)
                    nineRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.sixteenPiecePuzzleSolvedCount;
                if (fourXfourPuzzleSolvedCountTextView != null)
                    fourXfourPuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.sixteenRecordSolveTime / 1000.0 + " sec.";
                if (sixteenRecordSolveTimeTextView != null)
                    sixteenRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.twentyfivePiecePuzzleSolvedCount;
                if (fiveXfivePuzzleSolvedCountTextView != null)
                    fiveXfivePuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.twentyfiveRecordSolveTime / 1000.0 + " sec.";
                if (twentyfiveRecordSolveTimeTextView != null)
                    twentyfiveRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.thirtysixPiecePuzzleSolvedCount;
                if (sixXsixPuzzleSolvedCountTextView != null)
                    sixXsixPuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.thirtysixRecordsSolveTime / 1000.0 + " sec.";
                if (thirtysixRecordSolveTimeTextView != null)
                    thirtysixRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.fourtyninePiecePuzzleSolvedCount;
                if (sevenXsevenPuzzleSolvedCountTextView != null)
                    sevenXsevenPuzzleSolvedCountTextView.setText(temp);

                temp = "" + commonVariables.fourtynineRecordsSolveTime / 1000.0 + " sec.";
                if (fourtysevenRecordSolveTimeTextView != null)
                    fourtysevenRecordSolveTimeTextView.setText(temp);

                temp = "" + commonVariables.imagesSaved;
                if (puzzlesSavedTextView != null) {
                    puzzlesSavedTextView.setText(temp);
                }

                temp = "" + commonVariables.blogLinksTraversed;
                if (blogLinksTraversedTextView != null) {
                    blogLinksTraversedTextView.setText(temp);
                }

                temp = "" + commonVariables.musicSaved;
                if (musicSavedTextView != null) {
                    musicSavedTextView.setText(temp);
                }
            }
        });
    }
}
