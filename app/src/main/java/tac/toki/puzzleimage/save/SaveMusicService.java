package tac.toki.puzzleimage.save;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v7.app.NotificationCompat;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import tac.toki.puzzleimage.Data;
import tac.toki.puzzleimage.MainActivity;
import tac.toki.puzzleimage.R;
import tac.toki.puzzleimage.common.CommonVariables;

public class SaveMusicService extends IntentService {

    public SaveMusicService() {
        super("SaveMusicService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            saveMusicTrack();
        }
    }

    private void serviceToast(final String message) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void serviceUpdateUI() {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                MainActivity main = (MainActivity) CommonVariables.getInstance().context;
                main.updatePuzzleStats();
            }
        });
    }

    private void saveMusicTrack() {
        boolean mExternalStorageAvailable;
        boolean mExternalStorageWriteable;

        // save current image to devices images folder
        String state = Environment.getExternalStorageState();
        // check if writing is an option
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            mExternalStorageAvailable = mExternalStorageWriteable = true;
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
            mExternalStorageAvailable = true;
            mExternalStorageWriteable = false;
        } else {
            // Something else is wrong. It may be one of many other
            // states, but
            // all we need
            // to know is we can neither read nor write
            mExternalStorageAvailable = mExternalStorageWriteable = false;
        }

        if (mExternalStorageAvailable && mExternalStorageWriteable) {
            // then write picture to phone
            File path = Environment
                    .getExternalStoragePublicDirectory(Environment.DIRECTORY_MUSIC);

            final String name = "Signal.mp3";

            File file = new File(path, name);

            // check for file in directory
            if (file.exists()) {
                serviceToast("MP3 already saved!");
            } else {
                try {
                    boolean b1 = path.mkdirs();
                    boolean b2 = path.exists();
                    // Make sure the Pictures directory exists.

                    if (b1 || b2) {
                        InputStream is = CommonVariables.getInstance().context.getResources().openRawResource(
                                Data.TRACK_01);

                        OutputStream os = new FileOutputStream(file);
                        byte[] data = new byte[is.available()];
                        is.read(data);
                        os.write(data);
                        is.close();
                        os.close();

                        MediaScannerConnection
                                .scanFile(
                                        CommonVariables.getInstance().context,
                                        new String[]{file.toString()},
                                        null,
                                        new MediaScannerConnection.OnScanCompletedListener() {
                                            @Override
                                            public void onScanCompleted(
                                                    String path, Uri uri) {
                                                // new track is in database now for use
                                                CommonVariables.getInstance().musicSaved++;
                                                serviceUpdateUI();
                                                createNotification(name);
                                            }
                                        });
                    } else {
                        serviceToast("Could not make/access directory.");
                    }
                } catch (IOException e) {
                    serviceToast("ERROR making/accessing directory.");
                }
            }
        } else {
            serviceToast("Directory not available/writable.");
        }
    }

    public void createNotification(String name) {
        CommonVariables.getInstance().notificationsId++;
        Uri uri;
        Cursor cursor;
        int column_index_data;
        String absolutePathOfImage;
        uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        cursor = CommonVariables.getInstance().context.getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        String path = "";
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            if (absolutePathOfImage.contains(name)) {
                path = absolutePathOfImage;
                break;
            }
        }

        cursor.close();

        NotificationCompat.Builder mBuilder;
        mBuilder =
                new NotificationCompat.Builder(CommonVariables.getInstance().context);

        mBuilder.setSmallIcon(R.drawable.ic_save_music);
        mBuilder.setContentTitle(name + " Saved");
        mBuilder.setContentText("Click to Hear!");
        mBuilder.setAutoCancel(true);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent();
        resultIntent.setAction(Intent.ACTION_VIEW);
        resultIntent.setDataAndType(Uri.parse("file://" + path), "audio/*");

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(CommonVariables.getInstance().context);
        // Adds the back stack for the Intent (but not the Intent itself)
        //stackBuilder.addParentStack(ImageActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) CommonVariables.getInstance().context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(CommonVariables.getInstance().notificationsId, mBuilder.build());
    }
}