package tac.toki.puzzleimage.save;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.NotificationCompat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import tac.toki.puzzleimage.Data;
import tac.toki.puzzleimage.ImageActivity;
import tac.toki.puzzleimage.MainActivity;
import tac.toki.puzzleimage.R;
import tac.toki.puzzleimage.common.CommonVariables;

/**
 * The save photo class allows for the user to save a high resolution version of
 * the puzzle they just solved.
 *
 * @author Rick
 */
public class SavePhoto {

    CommonVariables commonVariables = CommonVariables.getInstance();
    int currentImageToSave;

    public SavePhoto(final int currentImage) {
        currentImageToSave = currentImage;

        final Activity activity = (Activity) commonVariables.context;
        activity.runOnUiThread(new Runnable() {

            private boolean mExternalStorageAvailable = false;
            private boolean mExternalStorageWriteable = false;

            @Override
            public void run() {
                // save current image to devices images folder
                String state = Environment.getExternalStorageState();
                // check if writing is an option
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    // We can read and write the media
                    mExternalStorageAvailable = mExternalStorageWriteable = true;
                } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
                    // We can only read the media
                    mExternalStorageAvailable = true;
                    mExternalStorageWriteable = false;
                } else {
                    // Something else is wrong. It may be one of many other
                    // states, but
                    // all we need
                    // to know is we can neither read nor write
                    mExternalStorageAvailable = mExternalStorageWriteable = false;
                }

                if (mExternalStorageAvailable && mExternalStorageWriteable) {
                    // then write picture to phone
                    File path = Environment
                            .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);

                    final String name = commonVariables.context.getString(R.string.artist) + "_"
                            + currentImageToSave + ".jpeg";

                    File file = new File(path, name);

                    // check for file in directory
                    if (file.exists()) {
                        commonVariables.showToast("Photo Exists Already!");
                    } else {
                        try {
                            boolean b1 = path.mkdirs();
                            boolean b2 = path.exists();
                            // Make sure the Pictures directory exists.
                            if (b1 || b2) {

                                // get file into input stream
                                InputStream is = activity.getResources().openRawResource(
                                        Data.PICS[currentImageToSave]);

                                OutputStream os = new FileOutputStream(file);
                                byte[] data = new byte[is.available()];
                                is.read(data);
                                os.write(data);
                                is.close();
                                os.close();

                                MediaScannerConnection
                                        .scanFile(
                                                commonVariables.context,
                                                new String[]{file.toString()},
                                                null,
                                                new MediaScannerConnection.OnScanCompletedListener() {
                                                    @Override
                                                    public void onScanCompleted(
                                                            String path, Uri uri) {
                                                        // new image is in phone
                                                        // database now for use
                                                        commonVariables.imagesSaved++;
                                                        createNotification(name);
                                                        MainActivity main = (MainActivity) activity;
                                                        main.updatePuzzleStats();
                                                    }
                                                });
                            } else {
                                commonVariables.showToast("Could not make/access directory.");
                            }
                        } catch (IOException e) {
                            commonVariables.showToast("ERROR making/accessing directory.");
                        }
                    }
                } else {
                    commonVariables.showToast("Directory not available/writable.");
                }
            }
        });
    }

    public void createNotification(String name) {
        commonVariables.notificationsId++;
        Uri uri;
        Cursor cursor;
        int column_index_data;
        String absolutePathOfImage;
        uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;

        String[] projection = {MediaStore.MediaColumns.DATA,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME};

        cursor = commonVariables.context.getContentResolver().query(uri, projection, null,
                null, null);

        column_index_data = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        String path = "";
        while (cursor.moveToNext()) {
            absolutePathOfImage = cursor.getString(column_index_data);
            if (absolutePathOfImage.contains(name)) {
                path = absolutePathOfImage;
                break;
            }
        }

        cursor.close();

        NotificationCompat.Builder mBuilder;
        mBuilder =
                new NotificationCompat.Builder(commonVariables.context);

        mBuilder.setSmallIcon(R.drawable.ic_save_pic);
        mBuilder.setContentTitle(name + " Saved");
        mBuilder.setContentText("Click to See!");
        mBuilder.setAutoCancel(true);

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent();
        resultIntent.setAction(Intent.ACTION_VIEW);
        resultIntent.setDataAndType(Uri.parse("file://" + path), "image/*");

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(commonVariables.context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(ImageActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) commonVariables.context.getSystemService(Context.NOTIFICATION_SERVICE);
        // mId allows you to update the notification later on.
        mNotificationManager.notify(commonVariables.notificationsId, mBuilder.build());
    }
}