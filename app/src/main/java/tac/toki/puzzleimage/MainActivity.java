package tac.toki.puzzleimage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import org.codechimp.apprater.AppRater;

import tac.toki.puzzleimage.puzzle.PuzzleStatsFragment;

/**
 * The main activity of the application, sets the fragment placeholder to the container view. On resume it will start and animation for the user. Test new images by changeing value in AdjustablePuzzle Class line 99.
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Start the creating an instance of the placeholder fragment. Run the app rater library.
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main);

        AppRater.app_launched(this);
    }

    /**
     * Start logging for Facebook user's. Begin the screen animation for starting the puzzle game.
     */
    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * Stop facebook tracking events.
     */
    @Override
    protected void onPause() {
        super.onPause();
    }

    /**
     * Create the default main menu layout.
     *
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * The main menu option is to show the stats fragment if not already, depending on layout, this might not be an option.
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // use a switch to run the handler for each item selected
        switch (item.getItemId()) {
            case R.id.menu_stats:
                switchToStatsFragment();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Fragment switching is setting the stats fragment on top of the puzzle, this way when the user backs out of the stats the puzzle is still playable.
     */
    private void switchToStatsFragment() {

        PuzzleStatsFragment fragment = (PuzzleStatsFragment) getFragmentManager().findFragmentByTag(PuzzleStatsFragment.TAG);
        if (fragment == null) {

            //start the stats fragment
            getFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, PuzzleStatsFragment.newInstance(), PuzzleStatsFragment.TAG).addToBackStack("puzzle").commit();
        } else {
            getFragmentManager().popBackStack();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * Our listener for the puzzles being solved. A this time the stats should be
     * updated if showing and the listener has fired.
     */
    public void updatePuzzleStats() {
        PuzzleStatsFragment fragment = (PuzzleStatsFragment) getFragmentManager().findFragmentByTag(PuzzleStatsFragment.TAG);
        if (fragment != null) {
            // Call a method in the ArticleFragment to update its content
            fragment.updatePuzzleStats();
        }
    }
}