package tac.toki.puzzleimage;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import tac.toki.puzzleimage.puzzle.PuzzleFragment;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class PuzzleVisibleTest {

    @Rule
    public ActivityTestRule mActivityRule;

    public PuzzleVisibleTest() {
        mActivityRule = new ActivityTestRule<>(
                MainActivity.class);
    }

    @Test
    public void testPuzzleVisible() {
        getInstrumentation().waitForIdleSync();
        onView(withId(R.id.puzzleFragment)).check(matches(isDisplayed()));
    }
}