package tac.toki.puzzleimage;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import tac.toki.puzzleimage.puzzle.PuzzleFragment;

import static junit.framework.Assert.assertTrue;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MyMediaPlayerTest {

    @Rule
    public ActivityTestRule mActivityRule;

    public MyMediaPlayerTest() {
        mActivityRule = new ActivityTestRule<>(
                MainActivity.class);
    }

    @Test
    public void testMusicIsPlaying() {
        //getInstrumentation().waitForIdleSync();
        MainActivity mainActivity = (MainActivity) mActivityRule.getActivity();
        PuzzleFragment fragment = (PuzzleFragment) mainActivity.getFragmentManager().findFragmentByTag(PuzzleFragment.TAG);
        if (fragment != null) {
            //start the stats fragment
            MyMediaPlayer myMediaPlayer = fragment.myMediaPlayer;
            assertTrue(myMediaPlayer.mediaPlayer.isPlaying());
        }
    }
}