package tac.toki.puzzleimage;

import android.content.DialogInterface;
import android.test.ActivityInstrumentationTestCase2;
import android.view.KeyEvent;
import android.view.Menu;

import tac.toki.puzzleimage.puzzle.PuzzleFragment;
import tac.toki.puzzleimage.puzzle.PuzzleSurface;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ThreadingTests extends ActivityInstrumentationTestCase2<MainActivity> {

    MainActivity mainActivity;
    PuzzleFragment puzzleFragment;
    PuzzleSurface puzzleSurface;
    Menu menu;

    public ThreadingTests() {
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        mainActivity = getActivity();

        puzzleFragment = (PuzzleFragment) mainActivity.getFragmentManager().findFragmentByTag(PuzzleFragment.TAG);
        if (puzzleFragment != null) {
            puzzleSurface = puzzleFragment.puzzleSurface;
            menu = puzzleFragment.menu;
        }
    }

    public void testMainActivityNotNull() {
        assertNotNull("MainActivity is null", mainActivity);
    }

    public void testPuzzleFragmentNotNull() {
        assertNotNull("PuzzleFragment is null", puzzleFragment);
    }

    public void testPuzzleSurfaceNotNull() {
        assertNotNull("PuzzleSurface is null", puzzleSurface);
    }

    public void testMenuNotNull() {
        assertNotNull("Menu is null", menu);
    }

    public void testMenuPuzzleRecreate() {
        for (int i = 0; i < 5; i++) {
            getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
            getInstrumentation().runOnMainSync(new Runnable() {
                @Override
                public void run() {
                    menu.performIdentifierAction(
                            R.id.new_puzzle, 0);
                }
            });

            getInstrumentation().waitForIdleSync();
            getInstrumentation().runOnMainSync(new Runnable() {
                @Override
                public void run() {
                    puzzleSurface.dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
                }
            });

            getInstrumentation().waitForIdleSync();

            //set the new puzzle size to be from 2 - 7
            puzzleSurface.defaultPuzzleSize = "" + ((i % 6) + 2);
        }
    }

    public void testMenuPuzzleRecreateAndCloseDefaultCreate() {
        getInstrumentation().sendKeyDownUpSync(KeyEvent.KEYCODE_MENU);
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                menu.performIdentifierAction(
                        R.id.new_puzzle, 0);
            }
        });

        getInstrumentation().waitForIdleSync();
        getInstrumentation().runOnMainSync(new Runnable() {
            @Override
            public void run() {
                puzzleSurface.dialog.getButton(DialogInterface.BUTTON_POSITIVE).performClick();
            }
        });

        getInstrumentation().waitForIdleSync();
        mainActivity.finish();
    }
}