package tac.toki.puzzleimage;

import junit.framework.Test;
import junit.framework.TestSuite;

public class PuzzleTestSuite {
    public static Test suite() {
        TestSuite suite = new TestSuite(ApplicationTest.class.getName());
        for (int i = 0; i < 10; i++) {
            // $JUnit-BEGIN$
            suite.addTestSuite(ThreadingTests.class);
            // $JUnit-END$
        }
        return suite;
    }
}